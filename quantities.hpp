/*
 * AUTHOR: Lukas Gail
 * DATE: 2023-03-13
 *
 * This file contains the definition of the quantity type and its arithmetic operations.
 */

#pragma once

#include <iostream>
#include <cmath>
#include "dimension.hpp"

namespace units {
    template<typename dimension>
    struct quantity {
        double scalar;

        explicit quantity(double value) : scalar(value) {}

        /**
         * This version relies on C++ 20 concepts, which allow explicit definition of constraints for template parameters
         */
        auto sqrt_v1() const requires(rootable<dimension>) {
            return quantity<DimensionRoot::apply<dimension, 2>>(sqrt(scalar));
        }

        /**
         * This version also works in older c++ versions without concept-support
         */
        auto sqrt_v2() const {
            // There's sadly no way to get the name of a type at compile time, so the custom message is pretty generic
            // The actual compiler message provides more context.
            static_assert(IsDivisibleBy::apply<dimension, 2>, "Tried to take a root of a physical quantity with odd dimensions.");
            return quantity<DimensionRoot::apply<dimension, 2>>(sqrt(scalar));
        }

        auto operator+(quantity<dimension> other) const {
            return quantity<dimension>(scalar + other.scalar);
        }

        auto operator-(quantity<dimension> other) const {
            return quantity<dimension>(scalar - other.scalar);
        }

        template<typename other_dimension>
        auto operator*(quantity<other_dimension> other) const {
            return quantity<DimensionMult::apply<dimension, other_dimension>>(scalar * other.scalar);
        }

        template<typename other_dimension>
        auto operator/(quantity<other_dimension> other) const {
            return quantity<DimensionDiv::apply<dimension, other_dimension>>(scalar / other.scalar);
        }

        auto operator-() const {
            return quantity<dimension>(-scalar);
        }

        auto operator*(double factor) {
            // An alternative to this would be an implicit conversion from double to quantity<dimensionless>
            return quantity<dimension>(scalar * factor);
        }

        template<unsigned int exponent>
        auto pow() {
            return quantity<DimensionPow::apply<dimension, exponent>>(std::pow(scalar, exponent));
        }

        friend std::ostream &operator<<(std::ostream &stream, quantity<dimension> self) {
            stream << self.scalar;
            dimension::print(stream);
            return stream;
        }
    };

    units::quantity<units::length> operator ""_m(char const *x);

    units::quantity<units::time> operator ""_s(char const *x);
}