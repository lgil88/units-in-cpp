# Units in C++

This project (more like collection of snippets really) provides a very basic and largely incomplete implementation of a SI unit system to check unit correctness at compile time.
It statically restricts the application of sqrt on quantities with even dimensions. (See implementations of sqrt_v1 and sqrt_v2)

It only implements the purely physical aspect of dimensions, so Bequerell and Rounds per Minute are the same dimension.
It also does not implement SI prefixes.

- `dimension.hpp` Defines physical dimensions as a product of the 7 SI base units and provides operations and constraints.
- `quantities.[hc]pp` Defines quantities with doubles and implements arithmetic operations.
- `main.cpp` provides some examples.

Tested with
 - `g++-10 -std=c++20 main.cpp`
 - `clang++-10 -std=c++20 main.cpp`