/*
 * AUTHOR: Lukas Gail
 * DATE: 2023-03-13
 *
 * This file contains the implementation of the quantity-type and some related metafunctions.
 */

#pragma once

#include <iostream>

namespace units {
    template<typename dimension>
    struct quantity; // Forward declaration

    // Definition
    template<int seconds, int metre, int kilogram, int ampere, int kelvin, int mole, int candela>
    struct dimension_t {
        using self = dimension_t<seconds, metre, kilogram, ampere, kelvin, mole, candela>;

        /**
         * Prints a string representation of the unit represented by this type
         * @param stream
         */
        static void print(std::ostream &stream) {
            struct _ {
                // This is proof that C++ is stupid for still not having local functions...

                static void helper(std::ostream &stream, char const *str, int n) {
                    if (n == 0) return;
                    else if (n == 1) stream << str;
                    else stream << str << "^" << n;
                }
            };

            _::helper(stream, "s", seconds);
            _::helper(stream, "m", metre);
            _::helper(stream, "kg", kilogram);
            _::helper(stream, "A", ampere);
            _::helper(stream, "K", kelvin);
            _::helper(stream, "mole", mole);
            _::helper(stream, "cd", candela);
        }
    };

    // For some preconstructed dimensions check the bottom of this file

    // Metafunction (provided as a metafunction-class) to multiply two physical dimensions
    class DimensionMult {
        template<typename left, typename right>
        struct _impl; //Undefined primary template

        // Specialization to deconstract the dimension_t instances. Also serves as a type check to only allow instanced of dimension_t
        template<int l_seconds, int l_metre, int l_kilogram, int l_ampere, int l_kelvin, int l_mole, int l_candela,
                int r_seconds, int r_metre, int r_kilogram, int r_ampere, int r_kelvin, int r_mole, int r_candela>
        struct _impl<dimension_t<l_seconds, l_metre, l_kilogram, l_ampere, l_kelvin, l_mole, l_candela>,
                dimension_t<r_seconds, r_metre, r_kilogram, r_ampere, r_kelvin, r_mole, r_candela>> {
            // Add the exponents.
            using result = dimension_t<
                    l_seconds + r_seconds,
                    l_metre + r_metre,
                    l_kilogram + r_kilogram,
                    l_ampere + r_ampere,
                    l_kelvin + r_kelvin,
                    l_mole + r_mole,
                    l_candela + r_candela>;
        };

    public:
        template<typename left, typename right>
        using apply = _impl<left, right>::result;
    };

    // Metafunction (provided as a metafunction-class) to divide two physical dimensions
    class DimensionDiv {
        template<typename left, typename right>
        struct _impl; //Undefined primary template

        // Specialization to deconstract the dimension_t instances. Also serves as a type check to only allow instanced of dimension_t
        template<int l_seconds, int l_metre, int l_kilogram, int l_ampere, int l_kelvin, int l_mole, int l_candela,
                int r_seconds, int r_metre, int r_kilogram, int r_ampere, int r_kelvin, int r_mole, int r_candela>
        struct _impl<dimension_t<l_seconds, l_metre, l_kilogram, l_ampere, l_kelvin, l_mole, l_candela>,
                dimension_t<r_seconds, r_metre, r_kilogram, r_ampere, r_kelvin, r_mole, r_candela>> {
            using result = dimension_t<
                    l_seconds - r_seconds,
                    l_metre - r_metre,
                    l_kilogram - r_kilogram,
                    l_ampere - r_ampere,
                    l_kelvin - r_kelvin,
                    l_mole - r_mole,
                    l_candela - r_candela>;
        };

    public:
        template<typename left, typename right>
        using apply = _impl<left, right>::result;
    };

    // Metafunction (provided as a metafunction-class) to determine whether all exponenets of a given are divisible by an integer.
    // Used to check whether the root of a quantity can be taken
    struct IsDivisibleBy {
    private:
        static constexpr bool divisible(int x, int divisor) {
            return (x % divisor) == 0;
        }

        template<typename dimensions, unsigned int divisor>
        struct _impl; // Undefined

        template<int seconds, int metre, int kilogram, int ampere, int kelvin, int mole, int candela, unsigned int divisor>
        struct _impl<dimension_t<seconds, metre, kilogram, ampere, kelvin, mole, candela>, divisor> {
            static constexpr bool result =
                    divisible(seconds, divisor) &&
                    divisible(metre, divisor) &&
                    divisible(kilogram, divisor) &&
                    divisible(ampere, divisor) &&
                    divisible(kelvin, divisor) &&
                    divisible(mole, divisor) &&
                    divisible(candela, divisor);
        };

    public:
        template<typename dimensions, unsigned int divisor>
        static constexpr bool apply = _impl<dimensions, divisor>::result;
    };

    // Wrap the constraint in a concept. A concept is essentially a compile-time function Type -> bool
    template<typename dimensions>
    concept rootable = IsDivisibleBy::apply<dimensions, 2>;

    // Metafunction (provided as a metafunction-class) to take the n-th root of a physical dimension by dividing all exponents by n.
    // The check whether this is allowed is done elsewhere!
    struct DimensionRoot {
    private:
        template<typename dimensions, unsigned int n>
        struct _impl; // Undefined

        template<int seconds, int metre, int kilogram, int ampere, int kelvin, int mole, int candela, unsigned int n>
        struct _impl<dimension_t<seconds, metre, kilogram, ampere, kelvin, mole, candela>, n> {
            using result = dimension_t<seconds / n,
                    metre / n,
                    kilogram / n,
                    ampere / n,
                    kelvin / n,
                    mole / n,
                    candela / n>;
        };

    public:
        template<typename dimensions, unsigned int n>
        using apply = _impl<dimensions, n>::result;
    };

    // Metafunction (provided as a metafunction-class) to get the dimension to the n-th power
    struct DimensionPow {
    private:
        template<typename dimensions, unsigned int n>
        struct _impl; // Undefined

        template<int seconds, int metre, int kilogram, int ampere, int kelvin, int mole, int candela, unsigned int n>
        struct _impl<dimension_t<seconds, metre, kilogram, ampere, kelvin, mole, candela>, n> {
            using result = dimension_t<seconds * n,
                    metre * n,
                    kilogram * n,
                    ampere * n,
                    kelvin * n,
                    mole * n,
                    candela * n>;
        };

    public:
        template<typename dimensions, unsigned int n>
        using apply = _impl<dimensions, n>::result;
    };

    using dimensionless = dimension_t<0, 0, 0, 0, 0, 0, 0>;

    // Base dimensions
    using time = dimension_t<1, 0, 0, 0, 0, 0, 0>;
    using length = dimension_t<0, 1, 0, 0, 0, 0, 0>;
    using mass = dimension_t<0, 0, 1, 0, 0, 0, 0>;
    using current = dimension_t<0, 0, 0, 1, 0, 0, 0>;
    using temperature = dimension_t<0, 0, 0, 0, 1, 0, 0>;

    using area = DimensionMult::apply<length, length>;
    using volume = DimensionMult::apply<area, length>;
    using frequency = DimensionDiv::apply<dimensionless, time>;

    using velocity = DimensionDiv::apply<length, time>; // There is no operator overloading on type level :(
    using acceleration = DimensionDiv::apply<velocity, time>;
    using force = DimensionMult::apply<mass, acceleration>;
    using pressure = DimensionDiv::apply<force, area>;
    using density = DimensionDiv::apply<mass, volume>;

    using energy = DimensionMult::apply<force, length>;
    using power = DimensionDiv::apply<energy, time>;

    using charge = DimensionMult::apply<current, time>;
    using electrical_potential = DimensionDiv::apply<energy, charge>;
    using electrical_resistance = DimensionDiv::apply<electrical_potential, current>;
    using electrical_conductance = DimensionDiv::apply<dimensionless, electrical_resistance>;
    using capacitance = DimensionDiv::apply<charge, electrical_potential>;
}
