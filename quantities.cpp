/*
 * AUTHOR: Lukas Gail
 * DATE: 2023-03-13
 */

#include "quantities.hpp"
#include "dimension.hpp"
#include <cstdlib>

using namespace units;

quantity<length> units::operator ""_m(char const *x) {
    return quantity<length>(atof(x));
}

quantity<units::time> units::operator ""_s(char const *x) {
    return quantity<time>(atof(x));
}