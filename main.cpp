/*
 * AUTHOR: Lukas Gail
 * DATE: 2023-03-13
 *
 * The files in this folder provide a very basic and largely incomplete implementation of a SI unit system to check unit correctness at compile time.
 * It statically restricts the application of sqrt on quantities with even dimensions. (See implementations of sqrt_v1 and sqrt_v2
 *
 * It only implements the purely physical aspect of dimensions, so Bequerell and Rounds per Minute are the same dimension.
 * It also does not implement SI prefixes.
 *
 * Tested with
 *  g++-10 -std=c++20 main.cpp
 *  clang++-10 -std=c++20 main.cpp
 */


#include <iostream>
#include "dimension.hpp"
#include "quantities.hpp"


int main() {
    using namespace units;

    // The literal postfixes are defined in quantities.hpp and are provided as a convenience

    quantity<area> area = 3.0_m * 3.0_m; // The unit is checked at compile time

    //quantity<volume> volume = 3.0_m * 3.0_m; // error

    //auto x = 3_m + 3_s; // error

    (3.0_m * 3.0_m).sqrt_v1();              // This is fine
    //(3.0_m * 3.0_m * 3.0_m).sqrt_v1();    // This is not

    (3.0_m * 3.0_m).sqrt_v2();              // This is fine
    //(3.0_m).sqrt_v2();                    // This is not


    // A stone falls from the height of 10 meters on earth in a vacuum.
    // h(t) = h0 + 1/2 * g * t^2
    // Find t where h(t) = 0
    //  t = sqrt((h(t) - h0) * 2 / a)

    quantity<acceleration> g = (-9.81_m / 1_s / 1_s);
    quantity<length> h0 = 10_m;
    quantity<units::time> hit_ground = ((0_m - h0) * 2 / g).sqrt_v1();

    std::cout << "The stone hits the ground after " << hit_ground << "\n";

    quantity<volume> cubicle = (2_m).pow<3>();
    std::cout << cubicle << "\n";

    return 0;
}